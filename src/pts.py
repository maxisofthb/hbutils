import math
import random
########################################################################
class points3D:
    """"""

    #----------------------------------------------------------------------
    def __init__(self, x=0, y=0, z=0):
        """Constructor"""
        self.x = x
        self.y = y
        self.z = z


    #----------------------------------------------------------------------
    def __add__(self, other):
        """"""
        assert isinstance(other, points3D)

        return points3D(self.x + other.x, self.y + other.y , self.z + other.z)

    #----------------------------------------------------------------------
    def __radd__(self, other):
        """"""
        return __add__(self, other)

    def __iadd__(self, other):
        assert isinstance(other, points3D)
        self.x, self.y, self.z = self.x + other.x, self.y + other.y, self.z + other.z
        return self

    #----------------------------------------------------------------------
    def __sub__(self, other):
        """"""
        assert isinstance(other, points3D)

        return points3D(self.x - other.x, self.y - other.y , self.z - other.z)



    #----------------------------------------------------------------------
    def __str__(self):
        """"""
        return str((self.x, self.y, self.z,))

    #----------------------------------------------------------------------
    def __repr__(self):
        """"""
        return (self.x, self.y, self.z,)
    
    def __iter__(self):
        return iter((self.x, self.y, self.z,))


    def milieu(self, p):
        assert isinstance(p, points3D)
        return points3D((self.x + p.x) / 2, (self.y + p.y) / 2, (self.z + p.z) / 2)

    #----------------------------------------------------------------------
    def Distance(self, other):
        """"""
        assert isinstance(other, points3D)
        return math.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2 + (self.z - other.z) ** 2)

    #----------------------------------------------------------------------
    def CompoAleal(self, limitup=500, limitdown=None):
        """"""
        if limitdown != None:

            self.x = random.randint(limitdown, limitup)
            self.y = random.randint(limitdown, limitup)
            self.z = random.randint(limitdown, limitup)
        else:
            limitdown = -limitup
            self.x = random.randint(limitdown, limitup)
            self.y = random.randint(limitdown, limitup)
            self.z = random.randint(limitdown, limitup)

        return self
    
    def to2DTab(self):
        return [self.x,self.y]





########################################################################
class NuageDePoints:
    """"""

    #----------------------------------------------------------------------
    def __init__(self):
        """Constructor"""
        self.content = []

    #----------------------------------------------------------------------
    def __add__(self, point):
        """Use Nuage+Point To add"""
        assert isinstance(point, points3D)
        self.content.append(point)
        return self
    
    def __iter__(self):
        return iter(self.content)
    
    def __len__(self):
        return len(self.content)
    
    def __getitem__(self, key):
        return self.content[key]
    
    def quickOptimise(self):
        """Very simple and useless :)"""
        if not self.content:
            return
        pt_encour = self.content[0]
        pt_suivant = self.content[1]
        tmp_Tab = self.content

        ret_Tab = []
        ret_Tab.append(pt_encour)

        mindist = pt_encour.Distance(pt_suivant)
        tmp_Tab.remove(pt_encour)
        while tmp_Tab:



            for otherpt in tmp_Tab:
                if otherpt.Distance(pt_encour) < mindist:
                    mindist = otherpt.Distance(pt_encour)
                    pt_suivant = otherpt

            ret_Tab.append(pt_suivant)
            tmp_Tab.remove(pt_suivant)

            pt_encour = pt_suivant
            if not tmp_Tab:
                break
            pt_suivant = tmp_Tab[0]
            mindist = pt_encour.Distance(pt_suivant)

        self.content = ret_Tab
        return ret_Tab
    
    def to2DTab(self):
        ret = []
        for pts in self:
            ret.append(pts.to2DTab())
        
        return ret






if __name__ == '__main__':  
    could = NuageDePoints()
    pts1, pts2, pts3 = points3D(1, 2, 3), points3D(4, 5, 6), points3D(2, 4)
    could + pts1
    could + pts2
    could + pts3
    for i in range(50):

        could + points3D().CompoAleal()
    for pts in could:
        print pts
    print '-------------'
    could.quickOptimise()
    for pts in could.content:
        print pts
        
    print could.to2DTab()











