# -*- coding:utf-8 -*-
import numpy as np
from scipy.spatial.distance import pdist, squareform

from pts import NuageDePoints, points3D
def TSPsolve2d(NuagePoints, ITERATIONS=5000):
    """ Tente de Resoudre Travelling Salesman Problem.
    return les distances rangée et l'ordres """
    assert isinstance(NuagePoints, NuageDePoints)
    cities = len(NuagePoints)
    
    
    np.random.seed(seed=346466)
    locations = NuagePoints.to2DTab()
    #Berechnung der euklidischen Distanz zwischen allen m�glichen Stadtpaaren
    distances = squareform(pdist(locations, 'euclidean'))
    #print distances
    
    np.random.seed()
    
    ###########################################################################
    #                      Genetischer Algorithmus zur L�sung des TSP         #
    ###########################################################################
    
    #Definition der Konstanten f�r den GA
    POPSIZE = 16;
    CROSSPROP = 0.99;
    MUTPROP = 0.05;
    
    bestDist = np.zeros(ITERATIONS) #In diesem Array wird f�r jede Iteration die beste Distanz gespeichert
    #Erzeugen einer zuf�lligen Startpopulation
    population = np.zeros((POPSIZE, cities + 1))
    for j in range(POPSIZE):
            population[j, 0:cities] = np.random.permutation(cities)
            population[j, cities] = population[j, 0]
    #print population
    
    
    cost = np.zeros(POPSIZE)#Speichert die Kosten jedes Chromosoms der aktuellen Population
    #Berechnung der Kosten jedes Chromosoms
    
    
    ##################################################################################################
    for it in range(ITERATIONS):
    
            #1.Berechne Fitness der aktuellen Chromosomen#################################################
            for j, pop in enumerate(population):
                    cost[j] = 0
                    for z in range(cities):
                            cost[j] = cost[j] + distances[pop[z], pop[z + 1]]
    
            sortedIndex = cost.argsort(axis=0)#Indizees der nach ansteigenden Kosten sortierten Chromosomen
            sortedCost = cost[sortedIndex] #die ansteigend sortierten Kosten
            bestDist[it] = sortedCost[0]
            sortedPopulation = population[sortedIndex] #Sortierung der Population nach ansteigenden Kosten
            InvertedCost = 1 / sortedCost #Berechung des Nutzen (Fitness) aus den Kosten
            #InvertedCost enth�lt die berechneten Fitness-Werte
    
            if it % 100 == 0:
                    print int(it * 100 / ITERATIONS), " %"
                    #print InvertedCost[0]
                    #print sortedPopulation[0]
    
            #2.Selektion: Zuf�llige Auswahl von Chromosomen aus der Population####################
            #Mit dem folgenden Prozess wird gew�hrleistet, dass die Wahrscheinlichkeit f�r die
            #Selektion eines Chromosoms umso gr��er ist, je gr��er sein Nutzenwert ist.
            InvertedCostSum = InvertedCost.sum()
            rn1 = InvertedCostSum * np.random.rand()
            found1 = False
            index = 1
            while not found1:
                    if rn1 < InvertedCost[:index].sum(axis=0):
                            found1 = index
                    else:
                            index += 1
            found1 = found1 - 1
            equal = True
            while equal:
                    rn2 = InvertedCostSum * np.random.rand()
                    found2 = False
                    index = 1
                    while not found2:
                            if rn2 < InvertedCost[:index].sum(axis=0):
                                    found2 = index
                            else:
                                    index += 1
                    found2 = found2 - 1
                    if found2 != found1:
                            equal = False
            parent1 = sortedPopulation[found1]
            parent2 = sortedPopulation[found2]
            ########## parent1 und parent2 sind die selektierten Chromsomen##############################
    
    
    
            #3.Kreuzung####################################################################################
            crossrn = np.random.rand()
            if crossrn < CROSSPROP:
                    cp = np.ceil(np.random.rand() * cities)
                    head1 = parent1[:cp]
                    tailind = 0
                    tail1 = np.zeros(cities - cp + 1)
                    for a in range(cities):
                            if parent2[a] not in head1:
                                    tail1[tailind] = parent2[a]
                                    tailind += 1
                    tail1[-1] = head1[0]
                    head2 = parent2[:cp]
                    tailind = 0
                    tail2 = np.zeros(cities - cp + 1)
                    for a in range(cities):
                            if parent1[a] not in head2:
                                    tail2[tailind] = parent1[a]
                                    tailind += 1
                    tail2[-1] = head2[0]
                    child1 = np.append(head1, tail1)
                    child2 = np.append(head2, tail2)
            #child1 und child2 sind die Ergebnisse der Kreuzung###############################################
    
    
            #4. Mutation#########################################################################################
            mutrn = np.random.rand()
            if mutrn < MUTPROP:
                    mutInd = np.ceil(np.random.rand(2) * (cities - 1))
                    first = child1[mutInd[0]]
                    second = child1[mutInd[1]]
                    child1[mutInd[0]] = second
                    child1[mutInd[1]] = first
                    child1[-1] = child1[0]
    
            mutrn = np.random.rand()
            if mutrn < MUTPROP:
                    mutInd = np.ceil(np.random.rand(2) * (cities - 1))
                    first = child2[mutInd[0]]
                    second = child2[mutInd[1]]
                    child2[mutInd[0]] = second
                    child2[mutInd[1]] = first
                    child2[-1] = child2[0]
            #child1 und child2 sind die Resultate der Mutation################################################
    
    
    
            #5. Ersetze die bisher schlechtesten Chromosomen durch die neu gebildeten Chromosomen, falls die neuen
            #besser sind
            costChild1 = 0
            costChild2 = 0
            for z in range(cities):
                    costChild1 = costChild1 + distances[child1[z], child1[z + 1]]
                    costChild2 = costChild2 + distances[child2[z], child2[z + 1]]
            replace1 = False
            replace2 = False
            index = POPSIZE - 1
            while index > 0:
                    if sortedCost[index] > costChild1 and not replace1:
                            if not np.ndarray.any(np.ndarray.all(child1 == sortedPopulation, axis=1)):
                                    sortedPopulation[index] = child1
                            replace1 = True
                    elif sortedCost[index] > costChild2 and not replace2:
                            if not np.ndarray.any(np.ndarray.all(child2 == sortedPopulation, axis=1)):
                                    sortedPopulation[index] = child2
                            replace2 = True
                    if replace1 and replace2:
                            break
                    index = index - 1
            population = sortedPopulation
            ######################################Ende der Iteration#############################
    print "Best Distance : %s" % (bestDist[-1])
    return [locations[int(pop)] for pop in population[0]], map(int, population[0])


def reBuildNuageDePoints(NuagePoints, ordre):
    """Met a jour le Nuage de Points avec l'ordre specifier.
    Retourne le Nuage de Points (pour le fun)"""
    assert isinstance(NuagePoints, NuageDePoints)
    newcontent = []
    for nbr in ordre:
        newcontent.append(NuagePoints[nbr])
        
    NuagePoints.content = newcontent
    return NuagePoints
    
def couldOptimise(NuagePoints, Iteration=7500):
    assert isinstance(NuagePoints, NuageDePoints)
    NuagePoints.quickOptimise()
    reBuildNuageDePoints(NuagePoints, TSPsolve2d(NuagePoints, Iteration)[1])
    return NuagePoints



if __name__ == '__main__':  
    could = NuageDePoints()
    pts1, pts2, pts3 = points3D(1, 2, 3), points3D(4, 5, 6), points3D(2, 4)
    could + pts1
    could + pts2
    could + pts3
    for i in range(50):

        could + points3D().CompoAleal()
    for pts in could:
        print pts
    print '-------------'
    could.quickOptimise()
        
    for pts in could:
        print pts
    #for pts in reBuildNuageDePoints(could,TSPsolve2d(could,80000)[1]):
    #    print pts
    
    couldOptimise(could)
    for pts in could:
        print pts
        
#plt.subplot(122)
#plt.grid(True)
#plt.plot(range(ITERATIONS),bestDist)
#plt.show()
