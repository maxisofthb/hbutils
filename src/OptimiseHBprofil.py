'''
Created on 7 sept. 2012

@author: maxisoft
'''
from pts import NuageDePoints, points3D
import Dijkstra
Nuage = NuageDePoints()
import xml.etree.ElementTree as ET


def Main(file='input.xml'):
    tree = ET.parse(file)
    root = tree.getroot()
    #Search & parse to objet :
    for child in root.iter('Hotspot'):
        toparse = child.attrib
        
        Nuage + points3D(float(toparse['X']), float(toparse['Y']), float(toparse['Z']))
        child.set('X', '3.5')
    

    #Optimise It
    Dijkstra.couldOptimise(Nuage, int(raw_input("Nombre d'iteration ? (7500 par defaut)\n")))
        
    #Mod and rewrite it
    i = 0
    for child in root.iter('Hotspot'):
        curr_pt = Nuage[i]
        child.set('X', str(curr_pt.x))
        child.set('Y', str(curr_pt.y))
        child.set('Z', str(curr_pt.z))
        
        i += 1
    tree.write('output.xml')

if __name__ == "__main__":
    Main()
