'''
Created on 6 sept. 2012

@author: maxisoft
'''

DEBUG = 0

from pts import points3D, NuageDePoints


import sqlite3
conn = sqlite3.connect('gameobj.db')
c = conn.cursor()
Req = "SELECT CAST(replace(position_x, ',' , '.') AS FLOAT), CAST(replace(position_y, ',' , '.') AS FLOAT), CAST(replace(position_z, ',' , '.') AS FLOAT)+1 FROM gameobject Where id=? and map=? LIMIT 1,2500;"


Buffer = ""

Hotspot_str = '<Hotspot X="%s" Y="%s" Z="%s" />'

Profile_xml = """<HBProfile>
   <!--
      PROFILE EXPECTATIONS:
         * When using this profile, be certain to set the HonorBuddy bot to :
                (*) Grinding / Mixed

         * This profile is appropriate for:
                [X] Alliance
                [X] Horde      [X] Farming

         * An explanation of <ForceMail> and <ProtectedItems> usage is here...
               http://www.buddyforum.de/mediawiki/index.php?title=HonorBuddy:_About_ProtectedItems_and_ForceMail

    -->
   <Name>Auto Gen From Trinity DB profile</Name>
   <MinLevel>5</MinLevel>
   <MaxLevel>86</MaxLevel>

   <MinDurability>0.3</MinDurability>
   <MinFreeBagSlots>2</MinFreeBagSlots>

   <MailGrey>False</MailGrey>
   <MailWhite>True</MailWhite>
   <MailGreen>True</MailGreen>
   <MailBlue>True</MailBlue>
   <MailPurple>True</MailPurple>

   <SellGrey>True</SellGrey>
   <SellWhite>False</SellWhite>
   <SellGreen>False</SellGreen>
   <SellBlue>False</SellBlue>
   <SellPurple>False</SellPurple>

   <AvoidMobs>
   </AvoidMobs>

   <Blackspots>
   </Blackspots>

   <ContinentID>%s</ContinentID> <!--(optional tag, but good practice to include) -->

   <Mailboxes>
   </Mailboxes>

   <Vendors>
   </Vendors>

   <Hotspots>
   %s
   </Hotspots>

</HBProfile>"""

def GenBuffer(Map_id, obj_id,optimise=True):
    Nuage = NuageDePoints()
    print "Traitement de la requete..."
    param = (obj_id, Map_id,)
    global Buffer
    Buffer += "<!-- GameObjet : %s -->\n" % (obj_id)
    
    c.execute(Req, param)
    res = c.fetchall()

    for x, y, z in res:
        #print x, y, z
        #Optimis
        Nuage + points3D(x, y, z)
        for pt in Nuage.content:
            print pt
    if optimise:
        Nuage.quickOptimise()
    for pt in Nuage.content:
        Buffer += Hotspot_str % (str(pt.x).replace(',', '.'), str(pt.y).replace(',', '.'), str(pt.z).replace(',', '.')) + "\n"



def WriteAll():
    with open('OnlyHotspot.xml', 'w') as file:
        file.write(Buffer)
    with open('GenProfil.xml', 'w') as file:
        file.write(Profile_xml % (Map_id, Buffer))
    with open('AllSpot.xml', 'a') as file:
        file.write(Buffer)


if __name__ == '__main__':
    print """Rappel : 
        0 : azeroth
        1 : kalimdor
        530 : Outre Terre
        571 : Northendre"""
    Map_id = 530
    obj_id = 1732
    if not DEBUG:
        Map_id = int(raw_input("Map ID ?\n"))
        obj_id = int(raw_input("Id du gameobjet ?\n"))
        

    GenBuffer(Map_id, obj_id)
    WriteAll()

    pass
